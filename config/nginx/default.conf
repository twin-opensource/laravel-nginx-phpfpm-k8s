upstream phpfpm { 
    server svc-laravel-nginx-phpfpm-k8s-php:9000; 
}
server {

    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;
    server_name _;
    client_max_body_size 5M;
    
    root /var/www/web/public/;
    index index.php index.html index.htm;
    sendfile off;
    # charset of "Content-Type" response header field
    charset utf-8;

    # log settings
    # access_log off;
    access_log /dev/stdout;
    error_log  /dev/stderr error;

    # turn off access logs and prevents logging 
    # an error if robots.txt and favicon.ico are not found
    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    # check if a file or directory index file exists, 
    # else pass the request to the index.php as a query parameter.
    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    # tell NGINX to proxy requests to PHP FPM via the FCGI protocol
    location ~ \.php$ {
        try_files $uri /index.php =404;
        fastcgi_pass phpfpm;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

    # Expire rules for static content
    # cache.appcache, your document html and data
    location ~* \.(?:manifest|appcache|html?|xml|json)$ {
        expires -1;
        # access_log logs/static.log; # I don't usually include a static log
    }
    # Feed
    location ~* \.(?:rss|atom)$ {
        expires 1h;
        add_header Cache-Control "public";
    }
    # Media: images, icons, video, audio, HTC
    location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|htc)$ {
        expires 1M;
        access_log off;
        add_header Cache-Control "public";
    }
    # CSS and Javascript
    location ~* \.(?:css|js)$ {
        expires 1y;
        access_log off;
        add_header Cache-Control "public";
    }

    # block access to .htaccess files
    location ~ /\.ht {
        deny all;
    }

    location ~ /.well-known {
        allow all;
    }
}
