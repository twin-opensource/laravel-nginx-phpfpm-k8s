# laravel-nginx-phpfpm-k8s

โค็ดตัวอย่างของการใช้งาน Laravel บน k8s โดยเป็นการแยก service ออกมาทั้ง nginx และ phpfpm รายละเอียดเพิ่มเติม https://blog.twinsynergy.co.th/structure-laravel5-nginx-phpfpm-k8s/

## Diagram

![diagram](https://blog.twinsynergy.co.th/content/images/2018/11/laravel.jpeg)

## วิธีใช้งาน

```bash
kubectl create -f k8s/
kubectl port-forward svc/svc-laravel-nginx-phpfpm-k8s-nginx 8080:80
```

จากนั้นเข้า `http://localhost:8080/`